(tool-bar-mode -1)
(toggle-scroll-bar -1)

(setq custom-file "~/.emacs.d/custom.el")
(setq doom-theme-file "~/.emacs.d/customize/doom-theme.el")
(setq init-dir (file-name-directory (or load-file-name (buffer-file-name))))
;;(org-babel-load-file (expand-file-name "loader.org" init-dir))
(setq global-linum-mode t)

(load custom-file)

(require 'package)
;;(require 'yaml-mode)

(defvar marmalade '("marmalade" . "https://marmalade-repo.org/packages/"))
(defvar melpa-stable '("melpa-stable" . "https://stable.melpa.org/packages/"))
(defvar melpa '("melpa" . "https://melpa.org/packages/"))
;;(defvar gnu '("https://elpa.gnu.org/packages/"))

(setq package-archives nil)
(add-to-list 'package-archives marmalade t)
(add-to-list 'package-archives melpa-stable t)
(add-to-list 'package-archives melpa t)
;;(add-to-list 'package-archives gnu t)
;;(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))
;;(add-to-list 'load-path "~/.emacs.d//")
(add-to-list 'load-path "~/.emacs.d/dl")

(defvar my-packages
  '(
    better-defaults
    paredit
    idle-highlight-mode
    ido-ubiquitous
    find-file-in-project
    magit
    smex
    scpaste
    doom-themes
    yaml-mode
    solaire-mode
    ;;nlinum-hl
    ))

(package-initialize)

(unless package-archive-contents
  (package-refresh-contents))
(unless (and (file-exists-p (concat init-dir "elpa/archives/marmalade"))
               (file-exists-p (concat init-dir "elpa/archives/melpa"))
               (file-exists-p (concat init-dir "elpa/archives/melpa-stable"))
	       ;;(file-exists-p (concat init-dir "elpa/archives/gnu"))
	       )
    (package-refresh-contents))



(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

(load doom-theme-file)
(load "nlinum-1.7.el")
(load "nlinum-hl.el")

(require 'nlinum)
(require 'nlinum-hl)
(require 'solaire-mode)

;; brighten buffers (that represent real files)
(add-hook 'after-change-major-mode-hook #'turn-on-solaire-mode)
